package com.example.alshaimaa_karam.bmi;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    EditText wt;
    EditText ht;
    Button show;
    double weight,height,bmi;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wt = (EditText) findViewById(R.id.weight_text);
        ht = (EditText) findViewById(R.id.height_text);
        show   =  (Button) findViewById(R.id.show);

        show.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                weight = Double.parseDouble(wt.getText().toString());
                height = Double.parseDouble(ht.getText().toString());
                bmi = weight /(height * height);


                if (bmi < 16)
                {
                    Log.d(" you are ", " seriouslyUnderweight ");
                }
                else if (bmi < 18)
                {
                  Log.d(" you are ", " Underwieght ");
                }
                else if (bmi < 24)
                {
                    Log.d(" you are ", " Normal ");
                }
                else  if (bmi < 29)
                {
                   Log.d(" you are ", " Overwieght ");
                }
                else  if (bmi < 35)
                {
                   Log.d(" you are ", " seriouslyOverweight ");
                }
                else  if (bmi > 35)
                {
                   Log.d(" you are ", " GravelyOverweight ");
                }
            }});
    }
}